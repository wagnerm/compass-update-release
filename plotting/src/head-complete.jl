using Plots
using Plots.PlotMeasures
using Plots.PlotUtils
using LaTeXStrings
using DelimitedFiles
using Printf

pyplot()

import PyCall
import PyPlot: matplotlib
using Plots.PlotThemes: palette

##############################################

const mytheme = :wong2;
function settheme()
    font =  Plots.font(8)
    fontt = Plots.font(10)
    theme(mytheme; guidefont=font, tickfont=font, legendfont=font, titlefont = fontt)
end

function mypalette()
    palette(mytheme);
end

# font
function setfonts()
    PyCall.PyDict(matplotlib["rcParams"])["font.family"] = "serif:bold"
    # PyCall.PyDict(matplotlib["rcParams"])["font.weight"] = "bold"
    PyCall.PyDict(matplotlib["rcParams"])["font.serif"] = "Helvetica" #"STIX"
    PyCall.PyDict(matplotlib["rcParams"])["mathtext.fontset"] = "dejavusans"#"Helvetica"
    PyCall.PyDict(matplotlib["rcParams"])["text.usetex"] = true
    # PyCall.PyDict(matplotlib["rcParams"])["text.latex.unicode"] = true
    # PyCall.PyDict(matplotlib["rcParams"])["text.latex.preamble"] = ["\\usepackage{siunitx}"];
end

function getsize(scale::Tuple)
    return 550 .* scale
end
function getsize(scale::Number)
    return getsize(scale, scale)
end
function getsize(xscale::Real, yscale::Real)
    return getsize((xscale, yscale))
end

settheme()
setfonts()
