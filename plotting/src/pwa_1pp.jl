include("head.jl")

label_name_map = [
                  ("h4001",latexstring("\$1^{++}\$ sum"),mypalette()[2]),
                  # ("h2",L"\rho\pi\,S",mypalette()[3]),
                  ("h11",L"(\pi\pi)_S\pi\,P",mypalette()[3]),
                  ("h16",L"f_0(980)\pi\,P",mypalette()[4])
                  #("h4080","3^{-+}"),
                  ]

tintervals = [("0.100", "0.113"),
              ("0.113", "0.127"),
              ("0.127", "0.144"),
              ("0.144", "0.164"),
              ("0.164", "0.189"),
              ("0.189", "0.220"),
              ("0.220", "0.262"),
              ("0.262", "0.326"),
              ("0.326", "0.449"),
              ("0.449", "0.724"),
              ("0.724", "1.000")]

function yscale(v)
    n=floor(log10(v))
    v / 10^n
end

function spin_sums(x, data, t1, t2; print_ticks_function = x->string(x))
    plot(size=getsize(0.5,0.5),
        xlab=L"m_{3\pi}\,(\mathrm{GeV}/c^2)",
        ylab=L"\mathrm{Number\,\,of\,\,events}\,/\,(20\,\mathrm{MeV}/c^2)",framestyle=:box)
    yscale = 10^5
    plot!(data[1][:,1],data[1][:,2]./yscale,α=0, lab="")
    for (k,(kf,v,c)) in enumerate(label_name_map)
        plot!(x,data[k][:,2]./yscale,seriestype=:stepbins, fill_between=fill(0,size(data[k],1)),
            lab=latexstring(v), lw=0.5,lc=:black, c=c, α=0.8)
    end
    uplim = plot!()[1][:yaxis][:extrema].emax*1.15
    #
    yticks = optimize_ticks(0.0, uplim; k_min = 4, k_max = 8, strict_span = true)[1]
    yticks_text = print_ticks_function.(yticks)
    plot!(yticks = (yticks,yticks_text))
    plot!(ylims=(0,uplim),
          xlim=(x[1],x[end]), xticks=0.5:0.5:2.5,
          tick_dir=:out,
          )
    annotate!([(0.55,0.96uplim,text(latexstring("$(t1)<t'<$(t2)\\,(\\mathrm{GeV}/c)^2"),8,:left))])
    ##
    plot!(inset=bbox(0.65,0.35,0.3,0.3))
    ## a11420 inset
    ymax = 1.3*max(data[end][:,2]...)/yscale
    #
    yticks = optimize_ticks(0.0, ymax; k_min = 4, k_max = 8, strict_span = true)[1]
    yticks_text = map(x->@sprintf("%.2f", x),yticks)
    plot!(yticks = (yticks,yticks_text), subplot=2)
    #
    plot!(subplot=2, xlim=(0.95,2.05), ylim=(0,ymax),
          xticks=collect(1:0.5:2),
          grid=false, legend=(0.58,0.9), tick_dir=:out)
    for (k,(fk,v,c)) in enumerate(label_name_map)
        (k==1) && continue
        data[k][:,[2,3]] ./= yscale
        bar!(subplot=2, data[k][:,1], data[k][:,2].+data[k][:,3],
            # seriestype=:stepbins,
            fill_between=(data[k][:,2] .- data[k][:,3]),
            lab="", lw=0, c=c, α=0.5)
        plot!(subplot=2, x,data[k][:,2],seriestype=:stepbins,
            lab="", lw=0.8,lc=c)
    end
    ##
    plot!()
end

function spin_sums(tslice; print_ticks_function = x->@sprintf("%.2f", x))
    data = [readdlm(joinpath("data","pwa_1pp","pwa.$(k).t$(tslice).txt")) for (k,v,c) in label_name_map]
    x = data[1][:,1]; dxh = (x[2]-x[1])/2
    x = [x[1]-dxh, (x.+dxh)...]
    #
    t1 = tintervals[tslice][1]; t2 = tintervals[tslice][2]
    spin_sums(x, data, t1, t2; print_ticks_function = print_ticks_function)
end

function shift_legend_add_scale_and_save(fname, lplt)
    savefig(fname)
    fig = lplt[1][1][:plot_object].o
    children = fig[:get_children]()
    ax = children[2]
    leg = ax[:get_legend]()
    leg[:set_bbox_to_anchor]((0.52,0.95))
    ax[:text](0, 1.0, L"\times 10^5", fontsize=8, transform=ax[:transAxes],
        verticalalignment="bottom")
    # ax[:yaxis][:set_tick_params](length=50, width=1)
    fig[:savefig](fname,bbox_inches = "tight")
end

# for ti = 3
#     lplt = spin_sums(ti)
#     # savefig(joinpath("gen-plots", "pwa_1pp_$(ti).pdf"))
#     shift_legend_add_scale_and_save(joinpath("gen-plots", "pwa_1pp_$(ti).pdf"), lplt)
# end

for ti = 1:11
    lplt = spin_sums(ti)
    # savefig(joinpath("gen-plots", "pwa_1pp_$(ti).pdf"))
    shift_legend_add_scale_and_save(joinpath("gen-plots", "pwa_1pp_$(ti).pdf"), lplt)
end

###################

function spin_sums_all()
    datas = [
        [readdlm(joinpath("data","pwa_1pp","pwa.$(k).t$(tslice).txt")) for tslice = 1:11]
            for (k,v,c) in label_name_map]
    data = sum.(datas)

    x = datas[1][1][:,1]; dxh = (x[2]-x[1])/2
    x = [x[1]-dxh, (x.+dxh)...]
    #
    t1 = tintervals[1][1]; t2 = tintervals[11][2]
    spin_sums(x, data, t1, t2; print_ticks_function=x->@sprintf("%.1f", x))
end

let
    lplt = spin_sums_all()
    # savefig(joinpath("gen-plots", "pwa_1pp_$(ti).pdf"))
    shift_legend_add_scale_and_save(joinpath("gen-plots", "pwa_1pp_sum.pdf"), lplt)
end
