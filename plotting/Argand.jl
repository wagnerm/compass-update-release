include(joinpath("src", "head-complete.jl"))
using Interpolations

function rel_error_prop_re(I,eI,ϕtot,eϕ)
    e1 = cos(ϕtot)*eI/(2I)
    e2 = sin(ϕtot)*eϕ
    return sqrt(e1^2+e2^2)
end
function rel_error_prop_im(I,eI,ϕtot,eϕ)
    e1 = sin(ϕtot)*eI/(2I)
    e2 = cos(ϕtot)*eϕ
    return sqrt(e1^2+e2^2)
end
arg(z) = atan(imag(z), real(z))

const tslicenames = [L"0.100 < t'\,/\,(\mathrm{GeV}/c)^2 < 0.113", L"0.113 < t'\,/\,(\mathrm{GeV}/c)^2 < 0.127", L"0.127 < t'\,/\,(\mathrm{GeV}/c)^2 < 0.144", L"0.144 < t'\,/\,(\mathrm{GeV}/c)^2 < 0.164", L"0.164 < t'\,/\,(\mathrm{GeV}/c)^2 < 0.189", L"0.189 < t'\,/\,(\mathrm{GeV}/c)^2 < 0.220", L"0.220 < t'\,/\,(\mathrm{GeV}/c)^2 < 0.262", L"0.262 < t'\,/\,(\mathrm{GeV}/c)^2 < 0.326", L"0.326 < t'\,/\,(\mathrm{GeV}/c)^2 < 0.449", L"0.449 < t'\,/\,(\mathrm{GeV}/c)^2 < 0.724", L"0.724 < t'\,/\,(\mathrm{GeV}/c)^2 < 1.000"]

const tslice = 0

function plot_argand(tslice; folder = "spin", title = L"\Delta\textrm{-}\mathrm{model\,\,(spin)}")
    #
    plot(title = L"\mathrm{Amplitude\,\,of\,\,the\,\,}J^{PC}M^\varepsilon=1^{++}0^{+}\,f_0\pi\,\,P\textrm{-}\mathrm{wave}", size=(360,350),
        grid=false, xlim=(-200,350), ylim=(-100,450), frame=:box, leg=:topright,
        ann=(-190,430,text(tslicenames[tslice+1], 9, :left)), top_margin = 3mm)
    #
    m_f0_bgd = readdlm(joinpath("amp_values",folder,"$(folder)_pssym_chi2sym_amp_f0piP_t$(tslice)_bgd"))
    m_f0_snl = readdlm(joinpath("amp_values",folder,"$(folder)_pssym_chi2sym_amp_f0piP_t$(tslice)_signal"))
    m_f0_sum = readdlm(joinpath("amp_values",folder,"$(folder)_pssym_chi2sym_amp_f0piP_t$(tslice)_sum"))
    #
    e = m_f0_snl[:,1]
    safefilt = 1.0 .< e .< 2.095  # theoretical calculation are done in the range 1.0 -- 2.095
    ffr = 1.3 .< e .< 1.8 # filt_fit_range
    #
    plot!(m_f0_sum[safefilt,2], m_f0_sum[safefilt,3], l=(:red, :dash) , lab="")
    plot!(m_f0_snl[safefilt,2], m_f0_snl[safefilt,3], l=(:blue, :dash), lab="")
    #
    plot!(m_f0_sum[ffr,2], m_f0_sum[ffr,3], l=(:red) , lab=title)
    plot!(m_f0_snl[ffr,2], m_f0_snl[ffr,3], l=(:blue), lab=L"\mathrm{signal}")
    #
    for (i,(v1,v2)) in enumerate(
        zip(m_f0_snl[ffr,2][1:9:end] .+ 1im .* m_f0_snl[ffr,3][1:9:end],
            m_f0_sum[ffr,2][1:9:end] .+ 1im .* m_f0_sum[ffr,3][1:9:end]))
         plot!([v1, v2], arrow=(0.8,0.5), l=(:green), lab=(i==1 ? L"\mathrm{backgound}" : ""))
     end
     #
     hline!([0.0], l=(0.5,:grey), lab="")
     vline!([0.0], l=(0.5,:grey), lab="")

    # data
    phi = readdlm(joinpath("tgraphs",folder,"$(folder)_pssym_chi2sym_t$(tslice)_graph_rhopiS_f0piP_data.dat"))
    f0pi = readdlm(joinpath("tgraphs",folder,"$(folder)_pssym_chi2sym_t$(tslice)_graph_f0piP_data.dat"))
    rhopi = readdlm(joinpath("tgraphs",folder,"$(folder)_pssym_chi2sym_t$(tslice)_graph_rhopiS_data.dat"))
    # error propogation
    phsp_f0pi = readdlm(joinpath("triangle_superposition", "QTBPSf0.dat"))
    itp_f0pi = interpolate((phsp_f0pi[:,1],), phsp_f0pi[:,2], Gridded(Linear()))
    phsp_rhopi = readdlm(joinpath("triangle_superposition", "QTBPSrho.dat"))
    itp_rhopi = interpolate((phsp_rhopi[:,1],), phsp_rhopi[:,2], Gridded(Linear()))
    #
    # modA_rhopi = sqrt.( rhopi[:,2] ./ itp_rhopi.(rhopi[:,1]) )
    modA_f0pi = sqrt.( f0pi[:,2] ./ itp_f0pi.(f0pi[:,1]) )
    #
    rel_phi_deg = phi[:,[2,3]] .* (π/180)
    #
    # a1 1260 model
    #
    m_rho_sum = readdlm(joinpath("amp_values",folder,"$(folder)_pssym_chi2sym_amp_rhopiS_t$(tslice)_sum"))
    rhopi_phase = [atan(y,x) for (x,y) in zip(m_rho_sum[:,2], m_rho_sum[:,3])]
    itp_rho_sum = interpolate((m_rho_sum[:,1],), rhopi_phase, Gridded(Linear()))
    #
    A_f0pi = modA_f0pi .* cis.(rel_phi_deg[:,1]) .* cis.(itp_rho_sum.(phi[:,1]))
    #
    Reerr = modA_f0pi .* [rel_error_prop_re(I,eI,ϕtot,eϕ) for (I,eI,ϕtot,eϕ) in zip(f0pi[:,2],f0pi[:,3],arg.(A_f0pi),rel_phi_deg[:,2])]
    Imerr = modA_f0pi .* [rel_error_prop_im(I,eI,ϕtot,eϕ) for (I,eI,ϕtot,eϕ) in zip(f0pi[:,2],f0pi[:,3],arg.(A_f0pi),rel_phi_deg[:,2])]
    #
    ed = f0pi[:,1]
    ffrd = 1.3 .< ed .< 1.8 # filt_fit_range
    #
    scatter!(A_f0pi,
        xerr=Reerr, yerr=Imerr,
        lab="", m=(0,stroke(0.8,:gray)))
    scatter!(A_f0pi[ffrd],
        xerr=Reerr[ffrd], yerr=Imerr[ffrd],
        lab="", m=(0,stroke(0.8,:black)))
    # points
    for i in 43:2:50#length(A_f0pi)-15
        v = round(ed[i], digits=2)
        scatter!([real(A_f0pi[i])], [imag(A_f0pi[i])], ann=(reim(A_f0pi[i]+8-18im)...,
            text(LaTeXString("\$$(v)\\,\\mathrm{GeV}\$"), 9, :left)),
            m=(4,:black,stroke(0)), lab=(i==43 ? L"m_{3\pi}\,\,\mathrm{indicated}" : ""))
        #
        iv = findfirst(x->x==v, m_f0_sum[:,1])
        scatter!([m_f0_sum[iv,2]], [m_f0_sum[iv,3]], m=(4,:red,stroke(0)), lab="")
    end
    # plot(m_rho_sum[:,1], rhopi_phase)
    plot!(xlab=L"\mathrm{Re}\,A", ylab=L"\mathrm{Im}\,A")
end

plot_argand(tslice; folder = "spin", title = L"\Delta\textrm{-}\mathrm{model\,\,(spin)}")
savefig(joinpath("plotting","argand_symspin_f0pi_t$(tslice).pdf"))

plot_argand(tslice; folder="bw", title=L"\mathrm{BW\textrm{-}model}")
savefig(joinpath("plotting","argand_symbw_f0pi_t$(tslice).pdf"))

for tslice in 0:10
    plot_argand(tslice; folder = "spin", title = L"\Delta\textrm{-}\mathrm{model\,\,(spin)}")
    savefig(joinpath("plotting","argand_symspin_f0pi_t$(tslice).pdf"))

    plot_argand(tslice; folder="bw", title=L"\mathrm{BW\textrm{-}model}")
    savefig(joinpath("plotting","argand_symbw_f0pi_t$(tslice).pdf"))
end
