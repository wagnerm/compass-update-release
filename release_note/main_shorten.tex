\documentclass[a4paper,12pt]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[]{csquotes}
\usepackage{amsmath,amssymb,amsthm}
\usepackage[varg]{txfonts}
\usepackage{graphicx}
\usepackage{float}
% \usepackage{ragged2e}
\usepackage{xspace}
% \usepackage{geometry}
\usepackage{pgffor}
\usepackage[left=2cm, right=2cm, top=2cm, bottom=2.5cm]{geometry}
\usepackage[nomessages]{fp}% http://ctan.org/pkg/fp

\usepackage[load=addn]{siunitx}
\sisetup{output-decimal-marker = {.}}
% \sisetup{separate-uncertainty}

\usepackage{ulem}
\usepackage{upgreek}

\usepackage[dvipsnames]{xcolor}
\usepackage{tikz}

\usepackage{hyperref}
% \usepackage{todonotes}
\usepackage{authblk}
\usepackage{overpic}

\input{mycommands.tex}
% Paper layout
% oddsidemargin = (21cm - textwidth)/2 - 2.54cm
% \textwidth10cm
% \oddsidemargin-0.54cm
% \textheight25.0cm
% \topmargin-2.5cm
% \parindent0.6cm
% \columnsep1.2cm
%
\graphicspath{{figs/}}

% \usepackage{geometry}
%  \geometry{
%  a4paper,
%  total={170mm,257mm},
%  left=20mm,
%  top=20mm,
%  }
\include{notations}
\include{review_tool}

\interfootnotelinepenalty=10000

% Literature
%  \usepackage[backend=biber, style=ieee, block=ragged, sorting=none]{biblatex}
% possible styles: ieee, science, ieee, nature, phys
%  \addbibresource{hadron.bib}
%  \addbibresource{a11420_note.bib}
%  \addbibresource{compass.bib}
%  \addbibresource{thesis_refs.bib}

% opening
\title{\textbf{-- COMPASS Release Note --}\\
Update on the mass-dependent fit of $\ao(1420)$ via\\ the ``Triangle Singularity''}

\author[1]{M.~Wagner~\thanks{mwagner@hiskp.uni-bonn.de}}
\author[2]{M.~Mikhasenko~\thanks{mikhail.mikhasenko@gmail.com}}
\author[1]{B.~Ketzer}
\author[2]{the hadron analysis subgroup}

\affil[1]{{Universit\"at Bonn,
Helmholtz-Institut f\"ur Strahlen- und Kernphysik, Bonn, Germany}}
%\affil[2]{Phys. Dept. E18, TU {M\"unchen}, Garching, Germany}
\affil[2]{CERN, Geneva, Switzerland}


\begin{document}

\maketitle

\begin{abstract}
This short note present a test of a new model for the calculation of the $a_1(1420)$-phenomenon via the rescattering processes using the COMPASS data.
% We intend to release plots of these studies involving a fit model (and the same program) that differ to the previously used.
We ask for a release of Figures~\ref{fig:fit_spin_BW},~\ref{fig:Argand_diagrams} and \ref{fig:full_fit_spin}, as well as all plots in appendix~\ref{app:fit_spin_BW}. The release of more plots specifically of the systematic studies will follow for the September analysis meeting.

Additional material including the fit results in form of root files and txt-files containing the parameters is provided via a git repository.
It can be cloned it with the command:
\begin{verbatim}
    git clone https://gitlab.cern.ch/wagnerm/compass-update-release.git
\end{verbatim}
% \\
% \verb|git clone https://gitlab.cern.ch/wagnerm/compass-update-release.git|\\
The repository also contains a pdf of the previous release note that can alos be found as Ref.~\cite{Wagner:2017xy}.
\end{abstract}

\section{Introduction}
This release note is intended as an update on the previous one ``Mass-dependent fit of the $\ao(1420)$ via the Triangle Singularity''~\cite{Wagner:2017xy}. Here we present a new method to calculate the triangle effects by applying the partial-wave-projection procedure to our case, which allows us to account for the correct quantum numbers of participating particles (spin of the $K^*$ and $J^{PC}=1^{++}$ for the overall system) and also yields another cross-check of the triangle-amplitude calculation.

% \mishatodo{
% on my side:
% \begin{itemize}
%     \item Work on text of the release note
% \end{itemize}

% on Mathias side:
% \begin{itemize}
%     % \item create a release note repository at Gitlab with all files and scripts that make plots
%     % \item check fit with the symmetrized integrals from \texttt{/mnt/data/compass/2008/integrals\_stefan/hXX.tc.out}
%     % \item Bootstrap fits with current Srijan's results
%     % \item Bootstrap of the MC integrals
%     % \item Estimation of $\rho\pi\,S$ projection
% \end{itemize}
% }

\subsection{Recapitulation}

In the note~\cite{Wagner:2017xy} we show the calculation of the triangle diagram (see Figure~\ref{fig:triangle_diagram}) using a dispersive approach. We present the fit model in detail and discuss why the rescattering interpretation of the $\ao(1420)$-signal is favored over a resonant interpretation, from an experimental as well as theoretical point of view.
Both models were used to fit the data and both yielded a similar quality of agreement with the data from the mass-independent partial-wave analysis.
\begin{figure}[H]
\centering
\raisebox{-0.5\height}{\includegraphics[width=0.45\textwidth]{figs/a1-cutting-1}}
\hfill
\raisebox{-0.5\height}{\includegraphics[width=0.5\textwidth]{{tprime-summed_intensity_1++_0+_f0_980_pi_P_with_model}}}
  \caption{Triangle diagram (left) as an explanation for the $\ao(1420)$-signal observed by COMPASS (right) \cite{Adolph:2015tqa} fitted here by a Breit-Wigner model.}
  \label{fig:triangle_diagram}
\end{figure}
In the previous note we presented the plots of the fit results for both triangle and Breit-Wigner model,
as well as the corresponding Argand-diagrams of the $f_0\pi\,P$-wave. The theoretical calculations as well as the implementation of the fit function were cross-checked and a different wave set was fitted with the same program to show agreement with the mass-dependent fits of Ref.~\cite{Wallner:2017xyz}.

The fit model consists of the coherent sum of a signal model and a phenomenological background of the form
\begin{equation}
a\cdot\left(\frac{m_{3\pi}-\SI{0.5}{\GeV}}{\SI{0.5}{\GeV}}\right)^b \exp{\left[c(t^\prime)\tilde{p}^2\right]},
\end{equation}
with $\tilde{p}$ being the involved break-up momentum.
The signal model is a relativistic Breit-Wigner with energy dependent width for the $a_1(1260)$ in the $\rho\pi\,S$-wave, a Flatté-parametrization for the $a_2(1320)$ in the $\rho\pi\,D$-wave involving \SI{80}{\percent} $\rho\pi$ and \SI{20}{\percent} $\eta\pi$ for the width, and finally in the $f_0\pi\,P$-wave we use for the Breit-Wigner model a relativistic Breit-Wigner for the $a_1(1420)$ with constant width and in the triangle model the same $a_1(1260)$ amplitude as for the $\rho\pi\,S$-wave multiplied to the theoretically precalculated triangle amplitude (a sum of the amplitudes for the $(K^{*0}K^-K^+)$-triangle and the $(K^{*-}K^0\bar{K}^0)$-triangle).

For the $\rho\pi\,S$- and $\rho\pi\,D$-wave the exponential in the background is a second-order polynomial in $t^\prime$ and for the $f_0\pi\,P$-wave it is taken constant in $t^\prime$ to reduce the number of fit parameters (and it is not needed to be more flexible here).
The magnitude of this coherent sum of signal and background is then multiplied to the (non-symmetrized) quasi-two-body phase space of the corresponding partial wave. More details on the fit function can be found in the note~\cite{Wagner:2017xy}.


\subsection{What is new}
The main difference to the previous studies is the theoretical model for the fit with the triangle amplitude.
We performed a new calculation using a recently-developed partial-wave-projection method explained in the next section.
This method allows us to properly include the spin of the $K^*$  and the overall $J^{PC}$ quantum numbers into our calculations.
Another difference to before is that we increase the upper bound of the fit range for the $f_0\pi\,P$-wave from \SI{1.6}{\GeV} to \SI{1.8}{\GeV}. This is done in an attempt to describe the experimental data in a larger mass range than in the original publication, which did not take into the account the tail on the high-mass side.

In addition we want to release several systematic studies that were performed on the fit without spin. We show later that both fits are very similar, therefore these systematic studies also can be accounted for the proper case with spin.


% ██████  ██     ██ ██████
% ██   ██ ██     ██ ██   ██
% ██████  ██  █  ██ ██████
% ██      ██ ███ ██ ██
% ██       ███ ███  ██

\section{Partial-wave-projection method}

\subsection{Description and implementation of the method}
Consider the decay of a resonance into $\pi^-_1\pi^+_2\pi^-_3$. We call $s_{ij}$ the invariant mass of the $(\pi_i\pi_j)$-system. Then we can decompose the full decay amplitude $A$ as a function of kinematic variables $\tau$ into partial waves:
\begin{equation} \label{eq:isobar.model}
  A(\tau) = \sum_{w=(JMLS)} \Big[ F_w(s_{12}) Z_w^*(\Omega_{3,12}) + F_w(s_{23}) Z_w^*(\Omega_{1,23}) \Big].
\end{equation}

Here $F_w$ are the corresponding isobar amplitudes, where we omit $F_w(s_{31})$ because we do not expect isobars in the $(\pi^-\pi^-)$-channel. $Z_w$ are kinematical functions describing the angular dependence of the isobar amplitudes with $\Omega_{i,jk}$ being a shorthand notation of the two angles of the isobar in the $(\pi_j\pi_k)$-channel in the Gottfried-Jackson frame, and the two angles of the $\pi_j$ in the helicity frame of the isobar.

Now we project the full amplitude onto one of the partial waves by integrating over all angles $\Omega_{3,12}$ together with their kinematic function $Z_w$:

\begin{equation*}
  A_w = \int \d Z_w(\Omega_{3,12}) A(\tau) = F_w(s_{12}) + \hat{F}_w(s_{12})
\end{equation*}
with the contribution of the cross-channel defined as:
\begin{equation*}
  \hat{F}_w(s_{23}) := \int \d Z_w(\Omega_{1,23}) \sum_{w'} F_{w'}(s_{12}) Z_{w'}^*(\Omega_{3,12}).
\end{equation*}
%
By employing unitarity of $A_w$ we obtain the Khuri-Treiman equation for the isobar amplitude:
\begin{equation}\label{eqn:Khuri-Treiman}
  F_w(s_{23}) = t_S(s_{23}) \left[ \underbrace{C_w(s_{23})}_{\equiv C_w} + \frac{1}{2\pi} \int_{s_\text{thr}}^\infty \frac{\rho(\sigma) \hat{F}_w(\sigma)}{\sigma-s_{23}} \d \sigma \right].
\end{equation}
%
Here $t_S$ is the two-body scattering amplitude and we make the simplification that $C_w$ is independent of $s_{23}$. This equation can be solved iteratively:
\begin{eqnarray*}
F^{(i)}_w(\sigma) &=& t^{(i)}_S(\sigma) \left[ C^{(i)}_w + \frac{1}{2\pi} \int_{ s_\text{thr}}^\infty \frac{\rho(\sigma) \hat{F}^{(i)}_w(\sigma')}{\sigma'-\sigma} \d \sigma' \right]\\
  \hat{F}^{(i+1)}_w(\sigma) &=& \int \d Z^{(i+1)}_w(\sigma') \sum_{w'} F^{(i)}_{w'}(\sigma') Z^{(i)*}_{w'}(\sigma')
\end{eqnarray*}

Let us now apply this procedure on our case. We want that the $\ao(1260)$ decays directly into $K^*\bar{K}$ in an $S$-wave (without any contribution from the cross-channel, thus $\hat F \equiv 0$), which gives us
\begin{equation*}
  F^{(0)}_{K^*}(s_{12}) = t^{(0)}_1(s_{12}) C^{(0)}.
\end{equation*}
Since $K^*$ has spin 1 we use the model $t^{(0)}(s_{12}) = p_{12} \text{BW}_{K^*}(s_{12})$ and for $C^{(0)} = \text{BW}_{\ao}(s)$, the latter representing the direct decay of the $\ao$ as a function of the total invariant mass $s$ of the 3-particle system.

In the second step we now have to calculate the projection integral. Here we assume that we only have to consider one set of quantum numbers, because we are only interested in the contribution of the $(K^*\bar{K})$-channel to the $(f_0\pi)$-channel, leaving only one remaining term in the sum. This integration~\footnote{One can show that the integral over all 4 angles can be simplified to one integral over the invariant $s_{12}$, with the result being a function of $s_{23}$.} can only be performed numerically:
\begin{eqnarray*}
  \hat{F}^{(1)}_{f_0} (s_{23}) &=& \int \d Z^{(1)}_{f_0}(s_{12}) F^{(0)}_{K^*}(s_{12}) Z^{(0)*}_{K^*}(s_{12})\\
  &=:& \text{BW}_{\ao}(s)\cdot I(s_{23}).
\end{eqnarray*}
Since the Breit-Wigner of $\ao$ is independent of the integration variable, it can be pulled out of the expression.

For the final step we assume that we do not have a direct decay $\ao(1260)\to f_0\pi$ which means $C^{(1)}_{f_0}\equiv 0$ and for a particle with spin 0 we use the model $t^{(1)}_{f_0}(s_{23}) = \text{BW}_{f_0}(s_{23})$. This leads us to the (almost) final result:
\begin{equation*}
  F^{(1)}_{f_0}(s_{23}) = \text{BW}_{\ao}(s) \cdot \text{BW}_{f_0}(s_{23}) \cdot  \frac{1}{2\pi} \int \frac{\rho(\sigma) I(\sigma)}{\sigma-s_{23}}\d \sigma
\end{equation*}

This is not yet the final answer because we did a mistake at the very beginning. We know that the full amplitude $A(\tau)$ has to be analytic, or in other words singularity-free.

Since $Z(\Omega_{1,23})\sim 1/\sqrt{\lambda_{s1}}$ we therefore need $F_{f_0}\sim \sqrt{\lambda_{s1}}$ with $\lambda_{s1}:=\lambda(s,m_1^2,s_{23})$ to cancel the kinematic singularities of $Z$. But for the derivation of equation~\eqref{eqn:Khuri-Treiman} $F$ is not allowed to have branch cuts which is now the case.

The solution to this is, that we consider $G=F/\sqrt{\lambda_{s1}}$ as well as $\hat{G}=\hat{F}/\sqrt{\lambda_{s1}}$ during the derivation of \eqref{eqn:Khuri-Treiman}. With this our final equation slightly modifies to
\begin{equation*}
  F^{(1)}_{f_0}(s_{23}) = p_{s1} \cdot \text{BW}_{\ao}(s) \cdot \text{BW}_{f_0}(s_{23}) \cdot  \underbrace{\frac{\sqrt{s}}{\pi} \int \frac{\rho(\sigma) I(\sigma)/\sqrt{\lambda_{s1}}}{\sigma-s_{23}}\d \sigma}_{=:\M_\Delta}
\end{equation*}
where we define $p_{s1} = \sqrt{\lambda_{s1}}/(2\!\sqrt{s})$. The advantage of this is that the dispersive integral inside of $\M_\Delta$ is now suppressed strong enough to converge without a subtraction. And what we also see immediately is the expected behavior of a $P$-wave in the decay of $\ao\to f_0\pi$ because of the additional $p_{s1}$-factor. Our ``classical'' isobar shape $\text{BW}_{f_0}$ is now only modified by an additional factor of $\M_\Delta$. The momentum $p_{s1}$ is part of the Blatt-Weißkopf factor that is introduced later in the fit model.

We can now compare the resulting $\M_\Delta$ with the amplitude that we get from the scalar case, see Figure~\ref{fig:comparison_spin_scalar}. One can observe that the general features of both amplitudes are very similar. As expected, the position of the peak stays approximately at the same position. Also the phase is very similar. The difference is, that the spin-amplitude rises later and stronger, and falls off a bit slower.
\begin{figure}[H]
\centering\includegraphics[width=0.8\textwidth]{amplitude_spin_scalar.pdf}
  \caption{Comparison of the triangle amplitude in the scalar $K^*$ case (dashed) and the case with spin (solid). Depicted are real part (blue), imaginary part (yellow) and the magnitude (green) of the amplitude. The complex argument (red) is drawn as well, but in arbitrary units of the same scale for both cases.}
  \label{fig:comparison_spin_scalar}
\end{figure}

The amplitude calculated with the method described above corresponds to cutting the triangle diagram through $K\bar{K}$ and dispersively recovering the full amplitude afterwards. We can draw a similar diagram as for the antecedent note~\cite{Wagner:2017xy}, see Figure~\ref{fig:triangle_diagram_new_cut} (compare to the diagram on the left panel of Figure~\ref{fig:triangle_diagram}).

\begin{figure}[H]
\centering \includegraphics[width=0.45\textwidth]{figs/a1-cutting-2}
  \caption{New partial-wave-projection calculations correspond to cutting the triangle diagram in this way and recovering the amplitude dispersively. In the fit we also included the other possible charge configuration with $K^{*-}K^0\bar{K}^0$.}
  \label{fig:triangle_diagram_new_cut}
\end{figure}

% ███████ ██ ████████     ███████ ██████  ██ ███    ██
% ██      ██    ██        ██      ██   ██ ██ ████   ██
% █████   ██    ██        ███████ ██████  ██ ██ ██  ██
% ██      ██    ██             ██ ██      ██ ██  ██ ██
% ██      ██    ██        ███████ ██      ██ ██   ████

\section{Fit with the spin-model and comparisons}
In all cases we performed a fit to all 11 $t^\prime$-slices simultaneously of the $\rho\pi\,S$-wave, the $f_0\pi\,P$-wave and the $\rho\pi\,D$-wave intensities as well as there interference terms (real and imaginary part).

% \subsection{The spin-model vs the scalar model}
For this fit we use exactly the same (cross-checked) program as for the case without spin. The only difference is the ``theory-file'' that we read in and multiply to the $\ao(1260)$-propagator in the $f_0\pi\,P$-wave. The result can be found in Figure~\ref{fig:fit_spin_scalar} for the first $t^\prime$-slice. The only difference in between Breit-Wigner and triangle model appears in the $f_0\pi\,P$-wave. Both other waves ($\rho\pi\,S$-wave and the $\rho\pi\,D$-wave) use the same parametrization and they don't differ much between both fit attempts, showing that they are not strongly influenced by the model for the $f_0\pi\,P$-wave.
\begin{figure}[H]
\centering\includegraphics[width=\textwidth, page=1]{figs/f0piP_spin_scalar.pdf}
  \caption{Comparison of the fit with the model with a vector-$K^*$ (solid) to the simplified model with a scalar-$K^*$ (dashed) for the first $t^\prime$-slice.}
  \label{fig:fit_spin_scalar}
\end{figure}
As we can see the inclusion of the spin has almost no effect on the actual fit result. Interesting to note is, that the contribution of the background is smaller inside the peak region if one includes the spin. In the scalar case, the background was needed for higher masses to compensate the faster falling signal.

We intend to release the main fit with the triangle model that properly includes the $K^*$-spin and its comparison to the Breit-Wigner model as shown in Figure~\ref{fig:fit_spin_BW} (here only first $t^\prime$-slice) as well as the other $t^\prime$-slices in appendix~\ref{app:fit_spin_BW}. The corresponding Argand-diagrams for both models are shown in Figure~\ref{fig:Argand_diagrams}.
The fit results arranged in the stamp plot are shown in Figure~\ref{fig:full_fit_spin} with the other $t^\prime$-slices being in appendix~\ref{app:fit_spin_BW}.

\begin{figure}[H]
\centering\includegraphics[width=\textwidth, page=1]{figs/f0piP_spin_BW.pdf}
  \caption{\textbf{PLOTS FOR RELEASE:} Comparison of the fit with the model with a vector-$K^*$ (solid) to the Breit-Wigner model (dashed) for the first $t^\prime$-slice.}
  \label{fig:fit_spin_BW}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.47\textwidth]{figs/argand_bw_f0pi_t0.pdf}
  \includegraphics[width=0.47\textwidth]{figs/argand_spin_f0pi_t0.pdf}
  \caption{\textbf{PLOTS FOR RELEASE:}
  Fit of the complex amplitude (with the threshold factors and the barrier factors divided out) for the Breit-Wigner model (left plot) and for the triangle model (right plot).
  The black points are calculated from the results of the model-independent fit of Ref.~\cite{Adolph:2015tqa} using fit result for $\rho\pi\,S$-wave as a reference.
  The colored lines show the fit result for the full amplitude (red line) and two components: the signal (blue) and the background (green).
  The lines are solid in the fit interval, while they are dashed for the regions of extrapolation.
  }
  \label{fig:Argand_diagrams}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth, page=1]{figs/psnormal_chi2sym_spin.pdf}
    \caption{\textbf{PLOTS FOR RELEASE:} Fit with the spin-model in the $f_0\pi\,P$-wave. Simultaneously fitted are all 11 $t^\prime$-slices of the three waves $\rho\pi\,S$, $f_0\pi\,P$ and $\rho\pi\,D$ (diagonal from top to bottom) with the full fit model depicted in red and the separated contributions of signal (blue) and background (green) to it. On the off-diagonal are the complex phases of the relative interferences between the respective waves. The full fit model is depicted in orange here, because we actually fit real and imaginary part instead of the here shown phase. The dotted lines show the extrapolation of the fit function outside of the fit range. This plot shows only the first $t^\prime$-slice.}
    \label{fig:full_fit_spin}
\end{figure}

% \newpage
%  \printbibliography
\bibliographystyle{ieeetr}
\bibliography{a11420_note} % hadron, compass, thesis_refs


\newpage
%  █████  ██████  ██████  ███████ ███    ██ ██████  ██ ██   ██
% ██   ██ ██   ██ ██   ██ ██      ████   ██ ██   ██ ██  ██ ██
% ███████ ██████  ██████  █████   ██ ██  ██ ██   ██ ██   ███
% ██   ██ ██      ██      ██      ██  ██ ██ ██   ██ ██  ██ ██
% ██   ██ ██      ██      ███████ ██   ████ ██████  ██ ██   ██

\appendix

\section{Comparison of spin- and Breit-Wigner model, other $t^\prime$-slices}
\label{app:fit_spin_BW}

\foreach \n in {2,...,11}{
\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth, page=\n]{figs/f0piP_spin_BW.pdf}
  \caption{Comparison of the fit with the model with a vector-$K^*$ (solid) to the Breit-Wigner model (dashed) for $t^\prime$-slice \n.}
\end{figure}
}

\foreach \n in {2,...,11}{
\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth, page=\n]{figs/psnormal_chi2sym_spin.pdf}
    \caption{Fit with the spin-model for the $f_0\pi\,P$-wave, $t^\prime$-slice \n.}
\end{figure}
}
%
% \section{Comparison of the spin- and scalar-model fits}
% \label{app:fit_spin_scalar}
%
% \foreach \n in {2,...,11}{
%
% \begin{figure}[H]
% \centering\includegraphics[width=0.9\textwidth, page=\n]{figs/f0piP_spin_scalar}
%   \caption{Comparison of the fit with the model with a vector-$K^*$ (solid) to the simplified model with a scalar-$K^*$ (dashed) for $t^\prime$-slice \n.}
% \end{figure}
% }


\end{document}
