# COMPASS update release

## Files in this repository

The structure of the filenames is the following:  
  If the file starts with "bw_" it is the Breit-Wigner fit-model, otherwise the triangle model.  
  "ps" stands for the phase space model: "normal" is the normal, unsymmetrized quasi-two-body phase space,  
  "chi2XXX" is the name of the used chi^2 function, "sym" is the usual chi^2 function  
  "_spin" follows if the model includes the spin of K*  
  "_tYYY" is the tslice index from 0 till 10  

The .root files contain the stamp plot for the given tslice.  
The .pdf contains all 11 stamp plots.  
The .result file contains the fit parameters and the settings for the fit program as well as the chi^2-value.  

## Code to extract functions from the stamp plot root files:

Use this code, if you want to access a specific part of the stamp plot (data, fit funcitions, etc.)  

std::string filename = "put the filename here";  
// open the root file:  
TFile *froot = new TFile(filename.c_str(), "READ");  

// get the canvas from the file:  
TCanvas *c1 = (TCanvas *)froot->Get(froot->GetListOfKeys()->At(0)->GetName());  

// get the pad of the desired plot from the stamp plot:  
// XXX is  
//   0 for rhopiS-intensity,  
//   1 for f0piP-intensity,  
//   2 for rhopiD-intensity,  
//   3 for rhopiS-f0piP-phase,  
//   4 for rhopiS-rhopiD-phase,  
//   5 for f0piP-rhopiD-phase  
TVirtualPad *p2 = (TVirtualPad *)c1->GetListOfPrimitives()->At(XXX);  

// get the data points from the pad:  
// it is actually a TGraphErrors, so use this if you need it for more than simple plotting.  
TGraph *data = (TGraph *)p2->GetListOfPrimitives()->At(1);  

// get the multigraph containing the fit functions:  
TMultiGraph *mgraph = (TMultiGraph *)p2->GetListOfPrimitives()->At(2);  

// get the different graphs of the multigraph:  
// YYY is  
//   0 for the full bgd (green),  
//   1 for the bgd in fit-range,  
//   2 for the full signal (blue),  
//   3 for the signal in fit-range,  
//   4 for the full fit-function (red),  
//   5 for the fit-function in fit-range  
TGraph *g = (TGraph *)mgraph->GetListOfGraphs()->At(YYY);  

// The used colors are:  
//   red: kRed+1  
//   blue: kBlue-2  
//   green: kGreen+2  
// note that the phases full fit-function was plotted in orange, since it was not fitted directly, but real and imaginary part instead.  

